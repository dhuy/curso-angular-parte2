angular.module('parte2App')
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/login', {
        template: '<form-user></form-user>',
        controller: 'LoginCtrl'
      })
  }])
;
